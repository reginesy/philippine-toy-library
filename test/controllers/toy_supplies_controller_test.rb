require 'test_helper'

class ToySuppliesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get toy_supplies_new_url
    assert_response :success
  end

  test "should get index" do
    get toy_supplies_index_url
    assert_response :success
  end

  test "should get update" do
    get toy_supplies_update_url
    assert_response :success
  end

end
