require 'test_helper'

class DonationControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get donation_index_url
    assert_response :success
  end

  test "should get new" do
    get donation_new_url
    assert_response :success
  end

  test "should get show" do
    get donation_show_url
    assert_response :success
  end

end
