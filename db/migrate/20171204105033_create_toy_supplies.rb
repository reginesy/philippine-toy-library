class CreateToySupplies < ActiveRecord::Migration[5.1]
  def change
    create_table :toy_supplies do |t|
      t.references :toy_library, foreign_key: true
      t.references :toy, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
