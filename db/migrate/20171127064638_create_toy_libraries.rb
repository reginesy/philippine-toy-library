class CreateToyLibraries < ActiveRecord::Migration[5.1]
  def change
    create_table :toy_libraries do |t|
      t.string :name
      t.string :status
      t.references :organizer, foreign_key: true

      t.timestamps
    end
  end
end
