class CreateToyRequestItems < ActiveRecord::Migration[5.1]
  def change
    create_table :toy_request_items do |t|
      t.references :toy, foreign_key: true
      t.decimal :quantity
      t.timestamps
    end
  end
end
