class CreateDonations < ActiveRecord::Migration[5.1]
  def change
    create_table :donations do |t|
      t.references :toy_library, foreign_key: true

      t.timestamps
    end
  end
end
