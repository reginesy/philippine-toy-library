FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential 

# for postgres
RUN apt-get install -y libpq-dev postgresql postgresql-contrib

# for nokogiri
RUN apt-get install -y libxml2-dev libxslt1-dev

# for a JS runtime
RUN apt-get install -y nodejs

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
ADD Gemfile $APP_HOME/Gemfile
ADD Gemfile.lock $APP_HOME/Gemfile.lock

ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile \
  BUNDLE_JOBS=2 \
  BUNDLE_PATH=/bundle

RUN bundle install
RUN bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java
ADD . /app