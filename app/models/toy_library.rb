class ToyLibrary < ApplicationRecord
  belongs_to :organizer
  
  has_many :donations
  has_many :donation_items, :through => :donations
end
