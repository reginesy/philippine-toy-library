class ToyRequest < ApplicationRecord
	belongs_to :toy_library
	belongs_to :organizer

	has_many :toy_request_item
	has_many :toys, :through => :toy_request_item, :class_name => 'Toys'

	accepts_nested_attributes_for :toys
	accepts_nested_attributes_for :toy_request_item
end