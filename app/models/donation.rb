class Donation < ApplicationRecord
  belongs_to :toy_library
  has_many :donation_items, inverse_of: :donation
  
  accepts_nested_attributes_for :donation_items, :reject_if => :all_blank, :allow_destroy => true
end
