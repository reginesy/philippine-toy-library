class DonationItem < ApplicationRecord
  belongs_to :toy_library
  belongs_to :donation
end
