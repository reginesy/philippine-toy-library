class ToyRequestItem < ApplicationRecord
	belongs_to :toy
	belongs_to :toy_request

	accepts_nested_attributes_for :toy, :reject_if => :all_blank
end
