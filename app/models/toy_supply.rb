class ToySupply < ApplicationRecord
  belongs_to :toy_library
  belongs_to :toy
  has_many :toys
  accepts_nested_attributes_for :toys, :reject_if => :all_blank, :allow_destroy => true
end
