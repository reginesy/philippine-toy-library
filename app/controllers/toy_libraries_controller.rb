class ToyLibrariesController < ApplicationController
	before_action :set_toy_library, except: [:index, :new, :create]
	def index
    	@toy_libraries = ToyLibrary.all.order(updated_at: :desc)
 	end

  	def new
    	@toy_library = ToyLibrary.new
  	end

  	def create
    	@toy_library = ToyLibrary.new(toy_library_params)

    	# Do some processing

    	if @toy_library.save
      		redirect_to toy_libraries_path
    	else
	      	render :new
    	end
  	end

  	def show
  	end

  	def edit
    	# Same code from show method
  	end

  	def update
    	# Same code from show
    	if @toy_library.update(toy_library_params)
      	redirect_to toy_libraries_path
    	else
      	render :edit
    	end
  	end

  	def destroy
    	# Same code from show method
    	if @toy_library.status == 'ACTIVE'
    		@toy_library.update(status: 'CLOSED')
    	else
    		@toy_library.update(status: 'ACTIVE')
    	end
    	redirect_to toy_libraries_path
  	end

  	private
    def set_toy_library
      @toy_library = ToyLibrary.find_by(id: params[:id])
      redirect_to toy_libraries_path if @toy_library.nil?
    end

    def toy_library_params
      params.require(:toy_library).permit!
    end
end

