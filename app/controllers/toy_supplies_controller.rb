class ToySuppliesController < ApplicationController
    before_action :set_toy_library, only: [:new, :create]
    before_action :build_toys, only: [:create]

    def new
   	 @toy_supply = ToySupply.new
    end

    def edit
   	 @toy_supply = ToySupply.new(toy_supply_params)

    if @toy_supply.save
      redirect_to toy_supply_path
    else
      render :new
    end
    end

    private
   	 def set_toy_library
   		 @toy_library = ToyLibrary.find_by(id: [params[:id], params[:toy_library_id]])
   		 redirect_to toy_libraries_path, notice: 'Toy Library not found.' if @toy_library.nil?
   	 end

   	 def toy_library_params
   		 params.require(:toy_library).permit!
   	 end

   	 def build_toys
   		 if params[:toy_library][:toy_supplies_attributes].present?
   			 toy_ids = []
   			 params[:toy_library][:toy_supplies_attributes].each do |k, v|
   				 if !is_number?(v[:toy_id])
   					 params[:toy_library][:toy_supplies_attributes][k][:toy_attributes] = { name: v[:toy_id].downcase }
   				 end
   			 end
   		 end
   	 end
   	 
   	 def is_number? string
   	   true if Float(string) rescue false
   	 end
end

