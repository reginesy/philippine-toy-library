class DonationsController < ApplicationController
  
  def index
  	@donations = Donation.all.order(updated_at: :desc)
  end

  def new
  	@donation = Donation.new
  end

  def create
  	@donation = Donation.new(donation_params)
  	if @donation.save
  		redirect_to donations_path
  	else
  		render :new
  	end
  end

  def show
  end

  private
  def donation_params
    params.require(:donation).permit!
  end
end
