class OrganizersController < ApplicationController
  before_action :set_organizer, except: [:index, :new, :create]
  def index
  	@organizers = Organizer.order(updated_at: :desc)
  end

  def new
  	@organizer = Organizer.new
  end

  def create
    @organizer = Organizer.new(organizer_params)
    if @organizer.save
      redirect_to organizers_path
    else
      render :new
    end
  end

  def edit
    
  end

  def update
    if @organizer.update(organizer_params)
      redirect_to organizers_path()
    else
      render :edit
    end
  end

  private
    def set_organizer
      @organizer = Organizer.find_by(id: params[:id])
      redirect_to organizers_path if @organizer.nil?
    end
    def organizer_params
      params.require(:organizer).permit!
    end
end
