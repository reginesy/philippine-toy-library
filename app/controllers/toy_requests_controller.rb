class ToyRequestsController < ApplicationController
	before_action :set_toy_request, except: [:index, :new, :create]
  before_action :set_toy_library, only: [:new, :create]
  before_action :build_toys, only: [:create]

  def index
  	@toy_requests = ToyRequest.order(updated_at: :desc)
  end

  def new
    @toy_request = ToyRequest.new
  end

  def create
    @toy_request = ToyRequest.new(toy_request_params)

    if @toy_request.save
      redirect_to toy_requests_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
  	if toy_request.update(toy_request_params)
  		redirect_to toy_requests_path
  	else
  		render :edit
  	end
  end

  def destroy
    if toy_request.status == 'ACTIVE'
      @toy_request.update(status: 'CLOSED')
    else
      @toy_request.update(status: 'ACTIVE')
    end
    redirect_to toy_requests_path
  end

  private
    def set_toy_request
      @toy_request = ToyRequest.find_by(id: params[:id])
      redirect_to toy_requests_path, notice: 'Toy Request not found.' if @toy_request.nil?
    end

    def set_toy_library
      @toy_library = ToyLibrary.find_by(id: [params[:id], params[:toy_library_id]])
      redirect_to toy_libraries_path, notice: 'Toy Library not found.' if @toy_library.nil?
    end

    def toy_request_params
      params.require(:toy_request).permit!
    end

    def build_toys
      if params[:toy_request][:toy_request_items_attributes].present?
        toy_ids = []
        params[:toy_request][:toy_request_items_attributes].each do |k, v|
          if !is_number?(v[:toy_id])
            params[:toy_request][:toy_request_items_attributes][k][:toy_attributes] = { name: v[:toy_id].downcase }
          end
        end
      end
    end

    def is_number? string
      true if Float(string) rescue false
    end
end
