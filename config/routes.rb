Rails.application.routes.draw do

 

  devise_for :admins, skip: [:registrations]
  devise_scope :admins do
  	authenticated :admin do
  		resources :organizers
  		resources :toy_libraries do
  		resources :toy_supplies
  		end
  		resources :toy_requests, only: [:destroy]
  		root to: 'toy_libraries#index', as: :admin_authenticated_root
  	end
  end

  devise_for :organizers, skip: [:registrations]
  devise_scope :organizers do
  	authenticated  :organizer do
  		resources :toy_libraries, except: [:new, :create, :destroy] do
  			resources :toy_supplies, only: [:new, :create]
  			resources :toy_requests, only: [:new, :create]
  		end
  		resources :toy_requests, except: [:new, :create]
  		root to: 'toy_libraries#index', as: :organizer_authenticated_root
  	end
  end
  root to: 'publics#index'
  resources :donations
end
